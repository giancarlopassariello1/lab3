package com.example;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    /* @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    } */

    /*@Test
    public void test() {
        fail("Not yet implemented");
    }*/

    @Test
    public void echoShouldReturnTrue() {
        assertEquals("Testing echo method", 5, App.echo(5));
    }
    
    @Test
    public void oneMoreShouldBeTrue() {
        assertEquals("Testing oneMore method", 2, App.oneMore(1));
    }
}
